# Garbage collection without garbage collector

## Basic concepts
Very simple:
* if something is a primitive value, it will be put on the stack, when the scope ends, stack will be freed, thus memory is also freed.
* if something is a dynamic value (size cannot be resolved at the compiling time), the (effective) pointer to the value will be put on the stack, pointing to the value on the heap. When the scope ends, stack will be freed, it will then trigger a `drop` function of the pointer to free the memory on heap.

But, what if there is no pointer pointing to the heap memory anymore? What if there is more than one pointer pointing to the heap memory?

To solve this, Rust introduces a simple rule: **There can only be exactly one pointer pointing to the heap memory at a certain timepoint!**

They call the (effective) pointer "owner" of the heap memory, and the relationship between the pointer and the heap memory is called "ownership".

This converts the problem of "how to manage the memory", and/or "how to collect garbage", to the problem of "how to manage the life time of the ownership", which, in my opinion, is a genius idea.

## Ownership examples
A basic ownership example looks like this:
```rust
let s1 = String::from("hello world");
let s2 = s1; // <-- transfer ownership (there can only be exactly one ...)

println!("{}", s1); // nope! s1 doesn't have the ownership anymore.
```

And function will take the ownership, making Rust more functional:
```rust
fn take_ownership(s: String) {
  println!("{}", s);
}

fn main() {
  let s1 = String::from("hello world");
  take_ownership(s1); // <-- transfer ownership (there can only be exactly one ...)

  println!("{}", s1); // nope! s1 doesn't have the ownership anymore.
}
```

To overcome this, return the same value if we still want the ownership in the caller:
```rust
fn take_ownership_and_give_back(s: String) -> String {
  println!("{}", s);
  s // return will transfer the ownership to the outer scope.
}

fn main() {
  let s1 = String::from("hello world");
  let s1 = take_ownership_and_give_back(s1); // <-- transfer ownership (parameter) and return ownership (return value)
  
  println!("{}", s1); // ok
}
```

Or use references, which is mentioned below

## Reference
Reference is used to borrow the ownership temporarily, but not take it:
```rust
fn not_take_ownership(s: &String) {
  println!("{}", s);
}

fn main() {
  let s1 = String::from("hello world");
  not_take_ownership(&s1); // <-- does not transfer ownership, just borrows it
  
  println!("{}", s1); // ok
}
```

There is some implicit information attached to each reference, which is its scope:
```rust
let r = {
  let x = 5;
  &x;
}; // error! x getting dropped here, but still referenced!

println!("{}", r);
```

This can be understood like: this error is thrown by an implicit scope check. An extra piece of information is attached to the reference type which is its borrowed scope. If a reference in an inner scope is potentially used outside of it, then a compiling time error will be reported.

Consider the code below:
```rust
fn ref_scope_problem(s1: &String, s2: &String) -> &String {
  if s1.len() > s2.len() {
    s1
  } else {
    s2
  }
}

fn main() {
  let s1 = String::from("hello world");
  
  let result = {
    let s2 = String::from("Hello world!");
    ref_scope_problem(&s1, &s2) // the scope information can either from s1 or s2, what should compiler do?
  };

  println!("{}", result);
}
```

To fix this problem, use the power of generic:
```rust
// the scope information must be unified among s1, s2 and the return value:
fn ref_scope_problem<'scope>(s1: &'scope String, s2: &'scope String) -> &'scope String {
  if s1.len() > s2.len() {
    s1
  } else {
    s2
  }
}

fn main() {
  let s1 = String::from("hello world");
  
  let result = {
    let s2 = String::from("Hello world!");
    ref_scope_problem(&s1, &s2) // nope! s1 and s2 are not in the same scope.
  };

  println!("{}", result);
}
```

## Mutable Reference
A mutalbe reference means the original owned data can be modified by the reference.

Rust introduced a very interesting way of managing mutable data. An owner can only have one mutable reference at a time, immutable reference cannot be created before the mutable reference and then used afterwards. In this case, a "change from nowhere" is never going to happen.

```rust
fn main() {
  let s1 = String::from("hello world");
  let s2 = &s1;
  let s3 = &mut s1;

  // nope, s3 is mutable reference created after s2
  // original string potentially being changed, dangerous!
  println!("{}", s2);
}
```

More interestingly, the receiver of the function can be a mutable reference, which means:
```rust
fn main() {
  let s1 = String::from("hello world");
  let s2 = &s1;

  // nope, the receiver of `push_str` is a mutable reference
  // original string potentially being changed, dangerous!
  s1.push_str(", some extra information.");

  println!("{}", s2);
}
```
