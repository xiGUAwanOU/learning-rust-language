# Cheatsheet

[[_TOC_]]

----

## 1. Syntax

### 1.1. Data types

#### 1.1.1. Simple types
* Integer types: `i8`, `u8`, `i16`, `u16`, `i32`, `u32`, `i64`, `u64`, `i128`, `u128`, `isize`, `usize`.
* Integer literals: `98_222`, `0xff`, `0o77`, `0b1111_0000`, `b'A'` (`u8`, byte)
* Floating-point types: `f32`, `f64`
* Boolean type: `bool`
* Character type: `char` (4-bytes! Supporting CJK characters, emoji, etc.)

#### 1.1.2. Tuple
```rust
let tup: (i32, f64, u8) = (500, 3.14, 1);

let (x, y, z) = tup;

let five_hundred = tup.0;
let three_point_one_four = tup.1;
let one = tup.2;
```

#### 1.1.3. Array
```rust
let a: [i32; 5] = [1, 2, 3, 4, 5];
let first = a[0];
let second = a[1];
```

#### 1.1.4. Structure
Keyed structures:
```rust
struct User {
  username: String,
  email: String,
  active: bool,
  sign_in_count: u64,
}

let user1 = User {
  username: String::from("someusername123"),
  email: String::from("someone@example.com"),
  active: true,
  sign_in_count: 1,
};
```

Tuple structures:
```rust
struct Color(i32, i32, i32);

let black = Color(0, 0, 0);
```

Adding functions:
```rust
struct Rectangle {
  width: f32,
  height: f32,
}

impl Rectangle {
  fn new(width: f32, height: f32) -> Rectangle {
    Rectangle {
      width: width,
      height: height,
    }
  }

  fn area(&self) -> f32 {
    self.width * self.height
  }
}

let rect = Rectangle::new(3.0, 4.0);
println!(rect.area());
```

### 1.2. Variables, constants and mutability

#### 1.2.1. Variables and mutability
```rust
let a = 5;
let mut b = 5;
// a = 6; <-- wrong, a is not mutable
b = 6; // ok.
let a = 6; // ok, the original a is shadowed
```

#### 1.2.2. Constants
```rust
const THE_ANSWER = 42;
```

### 1.3. Multi-statements expression
The value of the expression is the last statement (or expression) in the block:
```rust
let a = {
  let b = 5;
  b + 1
}; // a is 6 now
```

### 1.4. Function
```rust
fn func_name(param1: type1, param2: type2, ...) -> return_type {
  // implementation...
}
```

Return keyword is optional:
```rust
fn five() -> i32 {
  return 5;
}

fn alt_five() -> i32 {
  5
}
```

### 1.5. Conditions

#### 1.5.1. If
```rust
if condition1 {
  implementation1;
} else if condition2 {
  implementation2;
} ... else {
  implementationN;
}
```

It can be also used as an expression
```rust
let s =
  if n % 2 == 0 {
    "even number"
  } else {
    "odd number"
  };
```

#### 1.5.2. Match

### 1.6. Loops

## 2. Built-in libraries

### 2.1. String
There are two types for strings in Rust:
* `str`: an immutable sequence of characters located somewhere (similar to `const char *` in C++ with an extra integer field storing its length).
* `String`: a extendable structure which stored a sequence of characters, located on heap (similar to `string` in C++).

The `str` type is mostly used in `&str` as a slice of a `String`.

### 2.2. Result
